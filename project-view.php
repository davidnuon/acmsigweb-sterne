<?php include_once("inc/constants.php"); ?>
<?php include_once("inc/functions.php"); ?>
<?php include("inc/header.php"); ?>

<div class="content-container" style=" min-height:800px">
	<div class="container_12">
		<div class="grid_12">
			<img src="img/bg.jpg" alt="" style="width:100%;">
		</div>
		<div class="grid_10 push_1">
			<h1>Lorem ipsum dolor sit amet, consectetur.</h1>
			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, ab ipsum perferendis fugiat voluptatum hic enim. Dolorum, dicta, ducimus, repellendus nostrum at tenetur nobis eum sequi iusto impedit explicabo optio.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, id, at, iusto, quis dolores earum quisquam ab consequatur nemo optio quasi dignissimos aspernatur expedita iure pariatur voluptates tempore! Animi, est?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, iusto, culpa molestias repellat libero sequi eaque ad voluptatum est minima adipisci nam neque amet dicta blanditiis magnam maiores fugit ea!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, alias quos dignissimos mollitia ipsam praesentium vero repellat amet sint! Suscipit, numquam, consequuntur voluptate eum dicta quos ea quod quibusdam voluptatibus.</p>
		</div>
		<div class="grid_8">
			<h2>This is a test</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, explicabo, minus quae quam sunt veniam quis sapiente eaque officiis voluptas fugit vel eveniet suscipit obcaecati dicta architecto accusantium officia praesentium!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, adipisci, nam earum fugit architecto autem vitae quasi illo voluptas ipsam. Aliquid doloribus eius ullam debitis cum asperiores autem quisquam eos.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis consectetur praesentium harum modi quibusdam aliquid laboriosam necessitatibus laudantium hic! Voluptatum, similique, voluptas dolorem accusamus ad at cum corrupti quos deserunt.</p>
			<h2>This is a test</h2>
			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla, dolorem fugit rerum similique omnis quasi aliquid rem aspernatur officiis vero voluptatem alias provident pariatur excepturi odit incidunt soluta asperiores voluptates!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, in laudantium eius nisi officia hic ab dicta repudiandae quia necessitatibus nobis voluptatem. Dignissimos quod doloribus vel unde ipsa cupiditate aut.</p>
	<h2>This is a test</h2>
			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla, dolorem fugit rerum similique omnis quasi aliquid rem aspernatur officiis vero voluptatem alias provident pariatur excepturi odit incidunt soluta asperiores voluptates!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, in laudantium eius nisi officia hic ab dicta repudiandae quia necessitatibus nobis voluptatem. Dignissimos quod doloribus vel unde ipsa cupiditate aut.</p>
	<h2>This is a test</h2>
			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla, dolorem fugit rerum similique omnis quasi aliquid rem aspernatur officiis vero voluptatem alias provident pariatur excepturi odit incidunt soluta asperiores voluptates!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, in laudantium eius nisi officia hic ab dicta repudiandae quia necessitatibus nobis voluptatem. Dignissimos quod doloribus vel unde ipsa cupiditate aut.</p>
		</div>
		<div class="grid_4">
			<h2>Reasoning</h2>
			<h3>For all things.</h3>
			<img src="img/bg.jpg" alt="" style="width:100%;">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, obcaecati, sed a officia vitae quidem blanditiis nemo sunt id laboriosam impedit praesentium ut fugiat molestiae ea labore consequuntur nisi quia.</p>
			<h3>For all things.</h3>
			<img src="img/Shapes.jpg" alt="" style="width:100%;">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, obcaecati, sed a officia vitae quidem blanditiis nemo sunt id laboriosam impedit praesentium ut fugiat molestiae ea labore consequuntur nisi quia.</p>
		</div>
	</div>
</div>

<?php include("inc/breadcrumb.php"); ?>

<script>
	window.onload = function () {
		document.body.setAttribute('class', 'events-list-body')
	}
</script>
<?php include("inc/footer.php"); ?>
